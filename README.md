# Udemy Node JS Course - API, testing, mongodb, mongoose and heroku

## Introduction

This Todo app api was built whilst studying Andrew Meads highly recommended NodeJS course on Udemy<sup>1<sup>.  
The course introduces many facets of Nodejs programming including

- Express Server,
- Building APIs,
- Asynchronous NodeJS (including Promises and Async/Await)
- MongoDB and Mongoose
- Testing
- Socket.io

I can highly recommend this course and Andrew Meads enthusiastic and progressive teaching style.

I am publishing the final version of the API to provide a reference and as a proof of work for future projects and/or employment.

## The API

The API has a number of endpoints for access control and the CRUD of simple todos.

### GET /

Simple GET request to the root URL. Provides easy way to check app is up and running.

**Headers:** None

**JSON Body:** None

**Result Status:** Status 200

**Result Body:** "Example Todo App requires authenticated login!"

*See below section for API errors*

### POST /todos

Adds new todo via POST request for any authenticated user. Todo's are visible only to the user
who created them. JSON "text" field must contain at least one character otherwise a Todo validation error will be returned.

**Headers**
*Content Type:* application/json
*x-auth:* x-auth value from POST /users or POST /users/logi

**JSON body:**

```
{"text": "todo text"}
```

**Result Status:** Status 200

**Result Body:** JSON object

```
{
  "completed": false,
  "completedAt": null,
  "\_id": "5c153070174beb3ae491ea42",
  "text": "todo details",
  "\_creator": "5c1514cfc676233940d81d0a",
  "\_\_v": 0
}
```
*See below section for API errors*

### GET todos

Returns list of all Todos for currently logged in user. No JSON required. Logged in user auth is gained via header x-auth var when user logs in.

**Headers:**

*Content Type:* application/json

*x-auth:* x-auth value from POST /users or POST /users/lo

**JSON body:** x-auth value gained from POST /users or  POST /users/login

**Result Status:** Status 200 or 401 if x-auth not valid

**Result Body:** JSON Array of JSON objects

```
{
"todos": [
  {
    "completed": false,
    "completedAt": null,
    "_id": "5c153070174beb3ae491ea42",
    "text": "Goat Parkour",
    "_creator": "5c1514cfc676233940d81d0a",
    "__v": 0
  },
  {
    "completed": false,
    "completedAt": null,
    "_id": "5c1532b3174beb3ae491ea44",
    "text": "Goat Parkour",
    "_creator": "5c1514cfc676233940d81d0a",
    "__v": 0
  },
  {
    "completed": false,
    "completedAt": null,
    "_id": "5c1532ca174beb3ae491ea45",
    "text": "Dog athletics",
    "_creator": "5c1514cfc676233940d81d0a",
    "__v": 0
  },
  {
    "completed": false,
    "completedAt": null,
    "_id": "5c1532d6174beb3ae491ea46",
    "text": "Bunny Track",
    "_creator": "5c1514cfc676233940d81d0a",
    "__v": 0
  }
  ]
}
```

*See below section for API errors*

### GET todos:id

GET unique todos by _\_id_ where _\_id_ is the unique id of the document as shown in the above GET /todos/

**Headers**
*Content Type:* application/json
*x-auth:* x-auth value from POST /users or POST /users/lo

**Request** URL /todos/_\_id_

**JSON body:** None

**Results Status:** Status 200 

**Results Body:** Single JSON object for unique document ID

```
{
  "todo": {
    "completed": false,
    "completedAt": null,
    "_id": "5c1532d6174beb3ae491ea46",
    "text": "Bunny Track",
    "_creator": "5c1514cfc676233940d81d0a",
    "__v": 0
  }
}
```
*See below section for API errors*

## PATCH /todos:id

Update unique todos by _\_id_ where _\_id_ is the unique id of the document as shown in the above GET /todos/.  API call can update completed  and text values.

**Headers**
*Content Type:* application/json
*x-auth:* x-auth value from POST /users or POST /users/lo

**Request** URL /todos/_\_id_

**JSON body:** 
```
{
  "text": "updated todo text" *optional*
	"completed": true *optional*
}
```

**Results Status:** Status 200 

**Results Body:** Single JSON object for updated document
```
{
    "todo": {
        "completed": true,
        "completedAt": 1544905870749,
        "_id": "5c1564511444be43dc6902d6",
        "text": "Updated to cat bungee jumping",
        "_creator": "5c155d961444be43dc6902d0",
        "__v": 0
    }
}
```

*See below section for API errors*

### DELETE /todos:id
Delete a todo by ID.  Functionality limited to the deletion of 1 ID per API call.  Requires user authentication

Update unique todos by _\_id_ where _\_id_ is the unique id of the document as shown in the above GET /todos/.  API call can update completed  and text values.

**Headers**
*Content Type:* application/json
*x-auth:* x-auth value from POST /users or POST /users/lo

**Request** URL /todos/_\_id_

**JSON body:** 
None

**Results Status:** Status 200 

**Results Body:** Single JSON object for deleted document
```
{
    "todo": {
        "completed": true,
        "completedAt": 1544905870749,
        "_id": "5c1564511444be43dc6902d6",
        "text": "Updated to cat bungee jumping",
        "_creator": "5c155d961444be43dc6902d0",
        "__v": 0
    }
}
```

*See below section for API errors*

### POST /users
Creates a new user account with email address and password.  Email address must be valid and unique (no duplicates permitted).  Password must contain at least 6 characters.  The password is encrypted and salted on the database.  On transaction success the user will be logged in with an x-auth value in the returned header.  The x-auth value must be used for all other transactions as a unique session identifier.

**Headers:**
*Content Type:* application/json

**Request** URL /todos/_\_id_

**JSON body:** 
```
{
  "email":"*valid email address*",
  "password": "*6 characters or more*"
}
```

**Results Status:** 
Status 200

**Results Header:**
x-auth: *unique x-auth value to be used in subsequent transactions*

**Results Body:**
```
{
    "_id": "5c1587141444be43dc6902d7",
    "email": "test.alpha@test.com"
}
```

**Unique Email Error**
Status: 400

```
{
    "driver": true,
    "name": "MongoError",
    "index": 0,
    "code": 11000,
    "errmsg": "E11000 duplicate key error collection: TodoAppTest.users index: email_1 dup key: { : \"test.alpha@test.com\" }"
}
```

**Valid Password Error**
Status 400

```
{
    "errors": {
        "password": {
            "message": "Path `password` (`passw`) is shorter than the minimum allowed length (6).",
            "name": "ValidatorError",
            "properties": {
                "message": "Path `password` (`passw`) is shorter than the minimum allowed length (6).",
                "type": "minlength",
                "minlength": 6,
                "path": "password",
                "value": "passw"
            },
            "kind": "minlength",
            "path": "password",
            "value": "passw"
        }
    },
    "_message": "User validation failed",
    "message": "User validation failed: password: Path `password` (`passw`) is shorter than the minimum allowed length (6).",
    "name": "ValidationError"
}
```
*See below section for other API errors*

### POST /users/login
User login using email and password.  Returns unique x-auth value in header for use in subsequent transactions

**Headers**
*Content Type:* application/json

**Request** URL /users/login

**JSON body:** 
```
{
  "email": "*registered email address*"
	"password": "*registered password*" 
}
```

**Results Status:** 
Status 200 

**Results Body:** 
JSON object with User email and objectID

```
{
    "_id": "5c155d961444be43dc6902d0",
    "email": "test.beta@test.com"
}
```

*See below section for other API errors*

### GET /users/me
Get logged in users details using existing x-auth value in header

**Headers**
*Content Type:* application/json
*x-auth*: Logged in users x-auth value from previous log in or registration

**Request** URL /users/login

**JSON body:** 
```
{
  "_id": "*Users unique objectID*"
  "email": "*registered email address* 
}
```

**Results Status:** 
Status 200 

**Results Body:** 
JSON object with User email and objectID

```
{
    "_id": "5c155d961444be43dc6902d0",
    "email": "test.beta@test.com"
}
```



*See below section for other API errors*

### DELETE /users/me/token

Remove the x-auth token, effectivley logging the current user off of the API.  Requires x-auth token of logged in user.

**Headers**
*Content Type:* application/json
*x-auth:* x-auth value from POST /users or POST /users/lo

**JSON body:** 
None

**Results Status:** Status 200 

*See below section for API errors*

## API errors

### 400 bad request
- Check the url of your api request

### 401 authentication failed
- Check user is created, logged in and has x-auth token in header on creation or log in.  You need to use this x-auth header value in ALL GET, POST, PATCH, and DELETE API calls except GET /, POST /users and POST /users/login

### 404 page not found
- Todo or User id does not exist
- id is not a valid MondoDB ObjectID

### 5**  Servers or related services not available

**Result Error Body:**

```
{
"errors": {
  "text": {
    "message": "Path `text` is required.",
    "name": "ValidatorError",
    "properties": {
      "message": "Path `text` is required.",
      "type": "required",
      "path": "text",
      "value": ""
    },
    "kind": "required",
    "path": "text",
    "value": ""
  }
  },
"\_message": "Todo validation failed",
"message": "Todo validation failed: text: Path `text` is required.",
"name": "ValidationError"
}
```

## Versions

The master branch always contains the latest code release. Previous versions are contained in their own branch
The latest version to date is **V2**.

**V2** Updated Asynchronous processing using Async/Await functionality - in progress.

**V1** Todo API asynchronous app using nodejs promises

## Setup

**Install the following as global**

- node.js
- Gitbash (for github/bitbucket version control)
- MongoDb (including compass)
- node modules
  -- express
  -- nodemon

**Useful Software**

- VSCode (a great and free code editor)
- Postman - hugely useful API test app
- Chrome Browser - by far the best developer support of any major browser.

**Recommended**

- Heroku CLI (for free staging host)

## References

<sup>1</sup> https://www.udemy.com/the-complete-nodejs-developer-course-2/
