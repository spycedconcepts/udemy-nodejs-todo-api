Example Config for config.json

{
  "test": {
    "PORT": 5000,
    "MONGODB_URI": "mongodb://localhost:27017/dbNametest",
    "JWT_SECRET": "saltGoesHere"
  },
  "development": {
    "PORT": 3000,
    "MONGODB_URI": "mongodb://localhost:27017/dbNameDev",
    "JWT_SECRET": "saltGoesHere"
  }
}
