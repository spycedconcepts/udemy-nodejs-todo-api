/* 
    Example Nodejs TodoApp
    Copyright (C) 2018  Stuart last

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

**/

require("./config/config");

const {
  ObjectID
} = require("mongodb");
const express = require("express");
const bodyParser = require("body-parser");
const _ = require("lodash");

const {
  Mongoose
} = require("./db/mongoose");
const {
  Todo
} = require("./models/todo");
const {
  User
} = require("./models/user");
const {
  authenticate
} = require("./middleware/authenticate");

const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

//<<<< DEFINE ROUTES

//GENERICS
//========

app.get('/', (req, res) => {
  return res
    .status(200)
    .send("Example Todo App requires authenticated login!");
});

//TODOS
//=====

app.post("/todos", authenticate, (req, res) => {
  var todo = new Todo({
    text: req.body.text,
    _creator: req.user._id
  });

  todo.save().then(
    doc => {
      res.send(doc);
    },
    e => {
      res.status(400).send(e);
    }
  );
});

app.get("/todos", authenticate, (req, res) => {
  Todo.find({
    _creator: req.user._id
  }).then(
    todos => {
      res.status(200).send({
        todos
      });
    },
    e => {
      res.status(400).send(e);
    }
  );
});

// GET /todos/12345

app.get("/todos/:id", authenticate, (req, res) => {
  let id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Todo.findOne({
      _id: id,
      _creator: req.user._id
    })
    .then(todo => {
      if (!todo) {
        return res.status(404).send();
      }

      res.status(200).send({
        todo
      });
    })
    .catch(e => {
      res.status(400).send();
    });
});

app.delete("/todos/:id", authenticate, async (req, res) => {
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  try {
    const todo = await Todo.findOneAndDelete({
      _id: id,
      _creator: req.user._id
    });

    if (!todo) {
      return res.status(404).send();
    }

    res.status(200).send({
      todo
    });
  } catch (e) {
    res.status(400).send();
  }
});

app.patch("/todos/:id", authenticate, (req, res) => {
  let id = req.params.id;
  let body = _.pick(req.body, ["text", "completed"]);
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  if (_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime();
  } else {
    body.completed = false;
    body.completedAt = null;
  }

  Todo.findOneAndUpdate({
      _id: id,
      _creator: req.user._id
    }, {
      $set: body
    }, {
      new: true
    })
    .then(todo => {
      if (!todo) {
        return res.status(404).send();
      }

      res.status(200).send({
        todo
      });
    })
    .catch(e => {
      res.status(400).send();
    });
});

//USERS ROUTES
//=============

//Post New User

app.post("/users", async (req, res) => {
  try {
    const body = _.pick(req.body, ["email", "password"]);
    const user = new User(body);
    await user.save();
    const token = await user.generateAuthToken();
    res
      .status(200)
      .header("x-auth", token)
      .send(user);
  } catch (e) {
    res.status(400).send(e);
  }
});

// Get My User
app.get("/users/me", authenticate, (req, res) => {
  res.send(req.user);
});

// Login User {email, password}

app.post("/users/login", async (req, res) => {
  try {
    const body = _.pick(req.body, ["email", "password"]);
    const user = await User.findByCredentials(body.email, body.password);
    const token = await user.generateAuthToken();
    res
      .status(200)
      .header("x-auth", token)
      .send(user);
  } catch (e) {
    res.status(400).send();
  }
});

app.delete("/users/me/token", authenticate, async (req, res) => {
  try {
    await req.user.removeToken(req.token);
    res.status(200).send();
  } catch (e) {
    res.status(400).send();
  }
});

//>>>>>>END OF ROUTES
//++++++++++++++

app.listen(port, () => {
  console.log(`Started up app at port ${port}`);
});

module.exports = {
  app
};