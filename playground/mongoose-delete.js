/* 
    Example Nodejs TodoApp
    Copyright (C) 2018  Stuart last

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

**/

const {
  ObjectId
} = require("mongodb");

const {
  mongoose
} = require("../server/db/mongoose");
const {
  Todo
} = require("../server/models/todo");
const {
  User
} = require("../server/models/user");

id = "5be0c98ce8afa52ec0607ebd";

//Todo.remove

// Todo.deleteMany({}).then(result => {
//   console.log(result);
// }).catch((e) => {
//   console.log(e);
// });

// Todo.findOneAndDelete({id}).then((todo) => {
//   console.log(result);
// }).catch((e) => {
//   console.log(e);
// };

Todo.findByIdAndDelete(id)
  .then(todo => {
    console.log(todo);
  })
  .catch(e => {
    console.log(e);
  });