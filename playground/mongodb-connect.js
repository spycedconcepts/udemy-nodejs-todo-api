/* 
    Example Nodejs TodoApp
    Copyright (C) 2018  Stuart last

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

**/
const {
    MongoClient,
    ObjectID
} = require('mongodb');

// let obj = new ObjectID();
// console.log(obj);

MongoClient.connect('mongodb://localhost:27017/ToDoApp', {
    useNewUrlParser: true
}, (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    const db = client.db('ToDoApp');

    // db.collection('Todos').insertOne(({
    //     text: 'Something To Do',
    //     completed: false
    // }), (err, result) => {
    //     if (err) {
    //         return console.log('Unable to insert todo', err);
    //     }

    //     console.log(JSON.stringify(result.ops, undefined, 2));
    // });

    db.collection('Users').insertOne(({
        name: 'Stu Last',
        age: 49,
        location: 'UK'
    }), (err, result) => {
        if (err) {
            return console.log('Unable to insert user', err);
        }

        console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));

    });

    // db.collection('Todos').insertMany(([{
    //         text: "Eat lunch",
    //         completed: false
    //     },
    //     {
    //         text: "Eat Lunch",
    //         completed: false
    //     },
    //     {
    //         text: "Eat Lunch",
    //         completed: false
    //     },

    // ]), (err, result) => {
    //     if (err) {
    //         return console.log('Unable to add Todos');
    //     }
    // });

    //console.log(JSON.stringify(result.ops, undefined, 2));

    client.close();
});