/* 
    Example Nodejs TodoApp
    Copyright (C) 2018  Stuart last

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

**/
const {
    MongoClient,
    ObjectID
} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/ToDoApp', {
    useNewUrlParser: true
}, (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    const db = client.db('ToDoApp');

    //deleteMany
    // db.collection('Todos').deleteMany({
    //     text: "Eat Lunch"
    // }).then((result) => {
    //     console.log(result);
    // })

    //deleteOne
    // db.collection('Todos').deleteOne({
    //     text: "Eat Lunch"
    // }).then((result) => {
    //     console.log(result);
    // });

    //findOneAndDelete
    // db.collection('Todos').findOneAndDelete({
    //     completed: false
    // }).then((result) => {
    //     console.log(JSON.stringify(result, undefined, 2));
    // });

    //deleteMany

    db.collection('Users').deleteMany({
        name: "Stu Last"
    }).then((result) => {
        console.log(result);
    });


    //findOneAndDelete by ID
    db.collection('Users').findOneAndDelete({
        _id: new ObjectID('5bdf59412cd9c540b84badf7')
    }).then((result) => {
        console.log(JSON.stringify(result, undefined, 2));
    });


    client.close();
});