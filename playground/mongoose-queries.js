/* 
    Example Nodejs TodoApp
    Copyright (C) 2018  Stuart last

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

**/

const {
  ObjectId
} = require('mongodb');

const {
  mongoose,
} = require('./../server/db/mongoose');

const {
  Todo
} = require('./../server/models/todo');

const {
  User
} = require('./../server/models/user');

//Don't copy directly from Compass.  Paste from compass into notepad/notepad++ to remove invisible chars
//const id = "5be02f87424a6f1cc0a768f4";
//const id = "6be02f87424a6f1cc0a768f4";
//const id = "5be02f87424a6f1cc0a768f411";

// Todo.find({
//   _id: id
// }).then((todos) => {
//   if (todos.length == 0 || !todos) {
//     return console.log('Todos not found');
//   }
//   console.log('\n Todos: \n ============', todos);
// }).catch((e) => {
//   console.log('\n error: \n ============', e);
// });

// Todo.findOne({
//   _id: id
// }).then((todo) => {
//   if (!todo) {
//     return console.log('Id not found');
//   }
//   console.log('\n Todo: \n ============', todo);
// }).catch((e) => {
//   console.log('\n error: \n ============', e);
// });
// if (!ObjectID.isValid(id)) {
//   console.log(' ID not valid');
// }

// Todo.findById(id).then((todo) => {
//   if (!todo) {
//     return console.log('Id not found');
//   }
//   console.log('\n Todo by Id: \n ============', todo);
// }).catch((e) => {
//   console.log('\n error: \n ============', e);
// });

const id = "5be041a056f415316c81739b";
//const id = "6be041a056f415316c81739b";
//const id = "5be041a056f415316c81739b11";

//load in user model
//user findbyid
//query works but no user
//query works and is found
//handle errors.  don't worry about is valid.

User.findById(id).then((user) => {
  if (!user) {
    return console.log("User not found");
  }
  console.log(JSON.stringify(user, undefined, 2));
}).catch((e) => {
  console.log(e);
});